function populateFormSent(container){

    var container = "#" + container;

    var article = $("<article></article>");
    $(container).append(article);

    article.append("We've got your order:").append($("<br>"))
        .append(getParams());

}

function getParams() {
    var url_string = window.location.href;
    var url = new URL(url_string);

    return $("<p></p>").append("Title: ").append(url.searchParams.get("title")).append($("<br>"))
        .append("Name: ").append(url.searchParams.get("name")).append($("<br>"))
        .append("Surname: ").append(url.searchParams.get("surname")).append($("<br>"))
        .append("Email: ").append(url.searchParams.get("email")).append($("<br>"))
        .append("Gender: ").append(url.searchParams.get("gender")).append($("<br>"))
        .append("Ent chooser: ").append(url.searchParams.getAll("chooser")).append($("<br>"))
        .append("Subject: ").append(url.searchParams.get("subject")).append($("<br>"))
        .append("Royal color: ").append(url.searchParams.get("color")).append($("<br>"))
        .append("Kingdom: ").append(url.searchParams.get("kingdom")).append($("<br>"))
        .addClass("formSentParagraph");
}
