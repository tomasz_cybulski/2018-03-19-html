var ents = {
    "titleInfo": "Ents",
    "entsList": [
        {
            "name": "Fangorn",
            "otherName": "Treebeard",
            "birth": "Years of the Trees",
            "species": "beech-trees or oaks",
            "home": "Fangorn Forest",
            "phisicalDescription": [
                {
                    "hair": "leafy hair",
                    "height": "4.5m",
                    "skin": "bark-like",
                    "eyes": "deep brown shot"
                }
            ]
        },
        {
            "name" : "Finglas",
            "otherName" : "Leaflock",
            "birth" : "Years of the Trees",
            "species" : " oak-like",
            "home" : "Fangorn Forest",
            "phisicalDescription" : [
                {
                    "hair" : "leafy hair",
                    "height" : "4m",
                    "skin" : "bark-like",
                    "eyes" : "green"
                }

            ]
        },
        {
            "name" : "Fladrif",
            "otherName" : "Skinbark",
            "birth" : "Years of Sun",
            "species" : "white birch",
            "home" : "Fangorn Forest, slop of mountain",
            "phisicalDescription" : [
                {
                    "hair" : "leafy hair",
                    "height" : "5m",
                    "skin" : "white",
                    "eyes" : "dark"
                }

            ]
        },
        {
            "name" : "Bregalad",
            "otherName" : "Quickbeam",
            "birth" : "Second Age",
            "species" : "rowan tree",
            "home" : "Fangorn Forest",
            "phisicalDescription" : [
                {
                    "hair" : "leafy hair",
                    "height" : "4m",
                    "skin" : "dark brwon",
                    "eyes" : "red"
                }

            ]
        }
    ]
};

function populateEnts(container) {

    var container = document.getElementById(container);

    var article = document.createElement("article");
    container.appendChild(article);

    var title = createHeader('h2', ents.titleInfo);
    article.appendChild(title);

    var aside = document.createElement("aside");
    article.appendChild(aside);

    var text = document.createElement("section");
    text.id = "text";
    text.classList.add("text");
    var gif = createImg();
    gif.classList.add("gif");
    aside.appendChild(gif);
    aside.appendChild(text);

    var sectionEnts = document.createElement("section");
    article.appendChild(sectionEnts);

    var tableEnts = createTable(ents.entsList);
    sectionEnts.appendChild(tableEnts);

}

function createHeader(h, text) {

    var header = document.createElement("header");
    var h = document.createElement(h);

    header.classList.add("indexHeader");
    h.innerText = text;
    header.appendChild(h);

    return header;
}

function createTable(entList) {

    var table = document.createElement("table");
    table.classList.add("table");

    for (var element in entList) {

        var ent = entList[element];

        table.appendChild(createTableHeader(ent.name));
        table.appendChild(createSimpleRow("Other name", ent.otherName));
        table.appendChild(createSimpleRow("Birth", ent.birth));
        table.appendChild(createSimpleRow("Species", ent.species));
        table.appendChild(createSimpleRow("Home", ent.home));
        table.appendChild(createBiggerRow("Phisical description", ent.phisicalDescription));
    }

    return table;
}

function createSimpleRow(header, value) {

    var tr = document.createElement("tr");
    var th = document.createElement("th");

    th.innerHTML = header;
    tr.appendChild(th);

    var td = document.createElement("td");
    td.classList.add("tabletd");
    th.classList.add("tableth");
    td.innerHTML = value;
    tr.appendChild(td);

    return tr;
}

function createTableHeader(ent) {

    var tr = document.createElement("tr");
    var th = document.createElement("th");

    th.classList.add("tableHeader");
    tr.classList.add("tableHeader");

    th.colSpan = 2;
    th.innerText = ent;
    th.appendChild(createButton(ent));
    tr.appendChild(th);

    return tr;
}

function createBiggerRow(header, ent) {

    var tr = document.createElement("tr");
    var th = document.createElement("th");
    var td = document.createElement("td");
    var tableInner = document.createElement("table");
    tableInner.classList.add("tableInner");


    th.innerHTML = header;
    tr.appendChild(th);

    for (var element in ent) {
        tableInner.appendChild(createSimpleRow("hair", ent[element].hair));
        tableInner.appendChild(createSimpleRow("height", ent[element].height));
        tableInner.appendChild(createSimpleRow("skin", ent[element].skin));
        tableInner.appendChild(createSimpleRow("eyes", ent[element].eyes));
    }
    td.appendChild(tableInner);
    tr.appendChild(td);
    return tr;

}

function createImg() {

    var img = document.createElement("img");

    img.alt = "Ent picture";
    img.src = "img/tree.gif";
    img.classList.add("img");

    return img;
}

function createButton(ent) {

    var button = document.createElement("button");

    button.classList.add("button");
    button.innerHTML = "Add";

    var p  = document.createElement("p");
    p.classList.add("imgText");
    var section = document.getElementById("text");


    button.addEventListener("click", function () {
        p.innerHTML = ent;
    });

    section.appendChild(p);

    return button;
}


